package se.experis.noticeboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeboard.model.CommentEntry;
import se.experis.noticeboard.model.NoticeEntry;

/**
 * Author: Karl Löfquist
 * This class converts objects into database records and vice versa
 */
public interface CommentRepository extends JpaRepository<CommentEntry, Integer> {
    CommentEntry getById(String id);

}
