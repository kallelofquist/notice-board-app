package se.experis.noticeboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeboard.model.NoticeEntry;

/**
 * Author: Karl Löfquist
 * This class converts objects into database records and vice versa
 */
public interface EntryRepository extends JpaRepository<NoticeEntry, Integer> {
    NoticeEntry getById(Integer id);

}
