package se.experis.noticeboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeboard.Repositories.CommentRepository;
import se.experis.noticeboard.Repositories.EntryRepository;
import se.experis.noticeboard.model.CommentEntry;
import se.experis.noticeboard.model.CommonResponse;
import se.experis.noticeboard.model.NoticeEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Author: Karl Löfquist
 * Controller class for notice entries
 */
@Controller
public class EntryController {

    @Autowired
    private EntryRepository entryRepo;

    @Autowired
    private CommentRepository commentRepo;

    //List that is used to display all notice entries
    List <NoticeEntry> entryList = new ArrayList<>();

    //Fall-back if user accidentally reaches this page
    @GetMapping("/notice-entry")
    public ResponseEntity<CommonResponse> noticeRoot() {

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        cr.message = "Not implemented";

        HttpStatus resp = HttpStatus.NOT_IMPLEMENTED;

        return new ResponseEntity<>(cr, resp);
    }

    //When the user enters the start page, the user will reach this endpoint. Will display all entries.
    @GetMapping("/")
    public String getAllEntries(@CookieValue(value = "username", defaultValue = "") String usernameCookie, Model model) {
        //find all objects in the repository
        entryList = entryRepo.findAll();
        //sort it after date
        entryList.sort(new NoticeEntry());
        //Adding the "entries" list to thymeleaf
        model.addAttribute("entries", entryList);

        List<Boolean> matchingUsernameList = new ArrayList<>();

        for (NoticeEntry entry : entryList) {

            if (entry.userId.equals(usernameCookie)) {
                matchingUsernameList.add(true);
            }
            else {
                matchingUsernameList.add(false);
            }
        }

        model.addAttribute("showButtons", matchingUsernameList);

        //re-directs to the index.html page
        return "index";
    }

    //When the user clicks on the button "display", the user will reach this endpoint.
    @GetMapping("/notice-entry/display/{id}")
    public String displayEntry(@CookieValue(value = "username", defaultValue = "") String usernameCookie,
                               @PathVariable Integer id,
                               Model model) {

        List<CommentEntry> associatedComments = new ArrayList<>();

        CommonResponse cr = new CommonResponse();

        //Checking if the entry has a valid id
        if(entryRepo.existsById(id)) {
            Optional<NoticeEntry> repository = entryRepo.findById(id);
            NoticeEntry entry = repository.get();

            cr.data = entry;
            cr.message = "Found entry with id: " + entry.id;

            model.addAttribute("entry", entry);

            // Checking for associated comments
            for (CommentEntry comment : commentRepo.findAll()) {

                if (comment.noticeEntry == entry) {
                    associatedComments.add(comment);
                }

            }

            model.addAttribute("comments", associatedComments);

            // Check if the user is logged in and can comment
            if (!usernameCookie.isEmpty()) {
                model.addAttribute("userId", usernameCookie);
                model.addAttribute("canComment", true);
            }
            else {
                model.addAttribute("canComment", false);
            }

        } else {
            cr.message = "Entry not found with id: " + id;
        }

        //re-directs to the display page
        return "displayentry";
    }

    //When the user clicks on the edit button next to an entry, the user will reach this endpoint
    @GetMapping("/notice-entry/edit/{id}")
    public String editEntry(@PathVariable Integer id, Model model) {

        CommonResponse cr = new CommonResponse();

        //Checking if the entry has a valid id
        if(entryRepo.existsById(id)) {
            //find object in the repository
            Optional<NoticeEntry> repository = entryRepo.findById(id);
            NoticeEntry entry = repository.get();

            cr.data = entry;
            cr.message = "Updated entry with id: " + entry.id;

            //Adding the specific entry to thymeleaf
            model.addAttribute("entry", entry);

        } else {
            cr.message = "Entry not found with id: " + id;
        }
        //re-directs to the html page editentry
        return "editentry";
    }

    //When the user clicks on the add entry button, the user will reach this endpoint
    @GetMapping("/notice-entry/add")
    public String addEntry(@CookieValue(value = "username", defaultValue = "") String username, Model model) {

        if (!username.isEmpty()) {
            model.addAttribute("userId", username);
        }
        else {
            model.addAttribute("notLoggedIn", true);
        }
        //Re-directs the user to the html page addentry
        return "addentry";
    }

    //When the user clicks on the save button when creating an entry, the user will reach this endpoint
    @PostMapping("notice-entry/create-entry")
    public ResponseEntity<CommonResponse> createEntry(@RequestBody NoticeEntry entry){
        //if the entry is empty, return null
        if(!validateEntry(entry)) {
            return null;
        }
        else {
            System.out.println("Something went wrong!");
        }
        //process
        entry = entryRepo.save(entry);

        CommonResponse cr = new CommonResponse();
        cr.data = entry;
        cr.message = "New entry with id: " + entry.id;

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(cr, resp);
    }

    //If the user clicks on the edit button, the user will reach this endpoint
    @PatchMapping ("/notice-entry/{id}")
    public ResponseEntity<CommonResponse> updateEntry (@RequestBody NoticeEntry newEntry, @PathVariable Integer id) {
        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        //Checking if the entry has a valid id, and if true, check if the fields are empty. If not, set new value
        if(entryRepo.existsById(id)) {
            Optional<NoticeEntry> repository = entryRepo.findById(id);
            NoticeEntry entry = repository.get();

            if(newEntry.title != null) {
                entry.title = (newEntry.title);
            }
            if(newEntry.date!= null) {
                entry.date = (newEntry.date);
            }
            if(newEntry.textField != null) {
                entry.textField= (newEntry.textField);
            }
            if(newEntry.imageURL != null) {
                entry.imageURL = (newEntry.imageURL);
            }

            entryRepo.save(entry);

            cr.data = entry;
            cr.message = "Updated entry with id: " + entry.id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Entry not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, resp);
    }

    //If the user clicks on the delete button, the user will reach this endpoint
    @DeleteMapping("/notice-entry/{id}")
    public ResponseEntity<CommonResponse> deleteEntry(@PathVariable Integer id){
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        //If the user tries to delete a entry object with an existing id, delete
        if(entryRepo.existsById(id)) {
            Optional<NoticeEntry> repository = entryRepo.findById(id);
            NoticeEntry entry = repository.get();

            List<CommentEntry> associatedComments = new ArrayList<>();

            // Fetches all comments related to the post
            for (CommentEntry comment : commentRepo.findAll()) {

                if (comment.noticeEntry == entry) {
                    associatedComments.add(comment);
                }

            }

            // Deletes comments then the entry
            commentRepo.deleteAll(associatedComments);
            entryRepo.deleteById(id);

            cr.message = "Deleted entry with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Entry not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Checks so that no information is empty.
     * @param entry - Entry holding the information to be checked.
     * @return - True if valid else false.
     */
    private boolean validateEntry(NoticeEntry entry) {
        return (notNullOrEmpty(entry.date) && notNullOrEmpty(entry.date) && notNullOrEmpty(entry.textField));

    }

    private boolean notNullOrEmpty(String s) {
        return s != null && !s.equals("");
    }
}