package se.experis.noticeboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import se.experis.noticeboard.Repositories.CommentRepository;
import se.experis.noticeboard.Repositories.EntryRepository;
import se.experis.noticeboard.model.CommentEntry;
import se.experis.noticeboard.model.CommonResponse;

/**
 * Author: Karl Löfquist
 * Controller class for comments
 */
@Controller
public class CommentController {

    @Autowired
    private CommentRepository commentRepo;

    @Autowired
    private EntryRepository entryRepo;

    //When the user clicks on the "comment" button when displaying an entry, the user will reach this endpoint
    @PostMapping("notice-entry/create-comment")
    public ResponseEntity<CommonResponse> commentEntry(
            @RequestBody CommentEntry commentEntry,
            @RequestHeader(name ="notice-id") String noticeId){

        //if the entry is empty, return null
        if(!validateComment(commentEntry)) {
            System.out.println("Comment not valid");
            return null;
        }
        commentEntry.noticeEntry = entryRepo.getById(Integer.parseInt(noticeId));
        System.out.println("Posting on notice with id: "+commentEntry.noticeEntry.id);
        //process
        commentEntry = commentRepo.save(commentEntry);

        CommonResponse cr = new CommonResponse();
        cr.data = commentEntry;
        cr.message = "New entry with id: " + commentEntry.id;

        System.out.println(cr.message);

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Checks so that no information is empty.
     * @param entry - Entry holding the information to be checked.
     * @return - True if valid else false.
     */
    private boolean validateComment(CommentEntry entry) {
        return (notNullOrEmpty(entry.date) && notNullOrEmpty(entry.textField));

    }

    private boolean notNullOrEmpty(String s) {
        return s != null && !s.equals("");
    }

}
