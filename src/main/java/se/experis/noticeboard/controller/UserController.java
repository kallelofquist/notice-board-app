package se.experis.noticeboard.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import se.experis.noticeboard.Utils.SessionKeeper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Author: Karl Löfquist
 * Controller class for users
 */
@Controller
public class UserController {

    //If the user clicks on the log in link, the user will reach this endpoint
    @GetMapping("/login")
    public String redirectToLogin(@CookieValue(value = "username", defaultValue = "") String usernameCookie, Model model) {

        if (usernameCookie.isEmpty()) {
            model.addAttribute("loggedIn", false);
        }
        else{
            model.addAttribute("loggedIn", true);
        }

        return "login";
    }

    //If the user clicks on the log out link, the user will reach this endpoint
    @GetMapping("/logout")
    public String logout(@CookieValue(value = "username", defaultValue = "") String usernameCookie, HttpServletResponse response) {

        if (!usernameCookie.isEmpty()) {
            Cookie userNameCookieRemove = new Cookie("username", "");
            userNameCookieRemove.setMaxAge(0);
            response.addCookie(userNameCookieRemove);
        }

        return "redirect:/";
    }

    //If the user clicks on the log in button, the user will reach this endpoint
    @PostMapping("/loggingIn")
    public String login(@CookieValue(value = "username", defaultValue = "") String previousCookie,
                        HttpServletResponse response,
                        HttpSession session,
                        @RequestParam("username") String username,
                        @RequestParam("password") String password){

        if(!password.isEmpty() && !SessionKeeper.getInstance().CheckSession(session.getId())){
            SessionKeeper.getInstance().AddSession(session.getId());
        }
        else
        {
            return "redirect:/";
        }

        if(previousCookie.isEmpty()) {
            Cookie userCookie = new Cookie("username", username);
            userCookie.setMaxAge(600); // age in seconds, 10 minutes
            response.addCookie(userCookie);
        }

        return "loggedin";

    }

}
