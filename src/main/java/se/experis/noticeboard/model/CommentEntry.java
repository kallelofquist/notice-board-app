package se.experis.noticeboard.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.*;
import java.util.Comparator;

/**
 * Author: Karl Löfquist
 * Entity class for comments to Notice Board entries.
 * Sorts the comment entries after date
 *
 */
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class CommentEntry implements Comparator<CommentEntry> {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    @Column(nullable = false)
    public String userId;

    @ManyToOne
    public NoticeEntry noticeEntry;

    @Column(nullable = false)
    public String date;

    @Column(columnDefinition = "text")
    public String textField;


    //Method that sorts all diary entries after date
    @Override
    public int compare(CommentEntry o1, CommentEntry o2) {
        return o2.date.compareTo(o1.date);
    }
}