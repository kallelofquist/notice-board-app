package se.experis.noticeboard.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.*;
import java.util.*;

/**
 * Author: Karl Löfquist
 * Entity class for Notice Board entries.
 * Sorts the notice board entries after date
 *
 */
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class NoticeEntry implements Comparator<NoticeEntry> {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    @Column(nullable = false)
    public String title;

    @Column(nullable = false)
    public String date;

    @Column(nullable = false)
    public String userId;

    @Column(columnDefinition = "text")
    public String textField;

    @Column
    public String imageURL;

    @OneToMany(mappedBy = "noticeEntry",cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true )
    public Set<CommentEntry> comments = new HashSet<>();


    //Method that sorts all diary entries after date
    @Override
    public int compare(NoticeEntry o1, NoticeEntry o2) {
        return o2.date.compareTo(o1.date);
    }
}