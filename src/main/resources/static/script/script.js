
/**
*   Author: Karl Löfquist
*/

/*
*   Function for fetching entry information. When adding and editing, this function reads fields marked with
*   specific id:s to relate them to properties of a noticeEntry object.
*/
function fetchEntryInfo() {
    let  title = document.getElementById("title").value;
    let  date = document.getElementById("date").value;
    let  userId = document.getElementById("userId").value;
    let  textField = document.getElementById("textField").value;
    let  imageURL = document.getElementById("imageURL").value;

    if(date === "") {
        date = "2020-01-01"
    }

    let entry = {
        title: title,
        date: date,
        userId: userId,
        textField: textField,
        imageURL: imageURL
    };

    return entry;
}

/*
*   Function for fetching entry information. When adding comments, this function reads fields marked with
*   specific id:s to relate them to properties of a noticeEntry object.
*/
function fetchCommentInfo() {
    let  userId = document.getElementById("userId").value;
    let  entry = document.getElementById("entry").value;
    let  date = document.getElementById("date").value;
    let  textField = document.getElementById("textField").value;

    if(date === "") {
        date = "2020-01-01"
    }

    let commentEntry = {
        userId: userId,
        entry: entry,
        date: date,
        textField: textField
    };

    return commentEntry;
}

/*
*   Function for updating a notice entry. The function fetches information about the noticeEntry object and passes it on
*   to the /notice-entry/{id} endpoint for updating. Lastly, goes back to index page.
*/
function updateEntry(id) {
    const entry = fetchEntryInfo();

    entry.id = id;

    fetch("/notice-entry/" + entry.id, {
        method: "PATCH",
        body: JSON.stringify(entry),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(() => {
        window.location = "/";
    })
}

/*
*   Function for creating a notice entry. The function fetches information about the noticeEntry object and passes it on
*   to the /notice-entry/create-entry endpoint for crating an entry. Lastly, goes back to index page.
*/
function createEntry() {
    const entry = fetchEntryInfo();
    fetch("/notice-entry/create-entry", {
        method: "POST",
        body: JSON.stringify(entry),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(() => {
        window.location = "/";
    });
}

/*
*   Function for creating a comment entry. The function fetches information about the commentEntry object and passes it on
*   to the /notice-entry/create-comment endpoint for crating an entry. Lastly, reloads the page
*/
function createCommentEntry(entryId) {
    const commentEntry = fetchCommentInfo();
    fetch("/notice-entry/create-comment", {
        method: "POST",
        body: JSON.stringify(commentEntry),
        headers: {
            'Content-Type': 'application/json',
            'notice-ID': entryId
        },
    }).then(() => {
        location.reload();
    });
}

/*
*   Function for deleting a notice entry. It goes to the /notice-entry/{id} endpoint for deleting an entry.
*   Lastly, reloads the page.
*/
function deleteEntry(id) {
    fetch("/notice-entry/" + id, {method: "DELETE"}).then(res => {
        return res.json()
    }).then(() => {
        location.reload()
    });
}